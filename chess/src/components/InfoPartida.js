import React from 'react';
import DatosPartida from './DatosPartida';
import ControlPartida from './ControlPartida';

function InfoPartida({ socket, sala, rol }) {
    return (
        <>
            {sala && <DatosPartida {...sala} />}
            <ControlPartida socket={socket} sala={sala} rol={rol} />
        </>
    )
}

export default InfoPartida