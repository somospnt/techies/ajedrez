import { useEffect, useRef, useState } from 'react';
import Chess from 'chess.js';

import { Chessboard } from 'react-chessboard';

export default function PlayVsPlay({ boardWidth, socket, tablero }) {
    const chessboardRef = useRef();
    const [game, setGame] = useState(new Chess(tablero));
    const [color, setColor] = useState('');

    useEffect(() => {
        socket.on('color', setColor);
        socket.on('move', ({ sourceSquare, targetSquare }) => onDrop(sourceSquare, targetSquare));
        socket.on('reset', onClickReset);
        return () => {
            socket.off('move');
            socket.off('color');
            socket.off('reset');
        }
    }, []);

    function safeGameMutate(modify) {
        setGame((g) => {
            const update = { ...g };
            modify(update);
            return update;
        });
    }

    function onDrop(sourceSquare, targetSquare) {
        const gameCopy = { ...game };
        const move = gameCopy.move({
            from: sourceSquare,
            to: targetSquare,
            promotion: 'q'
        });
        setGame(gameCopy);
        move && socket.emit('move', { sourceSquare, targetSquare });
        move && socket.emit('tablero', gameCopy.fen());
        move && socket.emit('turno', gameCopy.turn());
        return move;
    }

    function onClickReset() {
        safeGameMutate((game) => {
            game.reset();
        });
        chessboardRef.current.clearPremoves();
    }

    return (
        <div>
            <Chessboard
                id="PlayVsPlay"
                animationDuration={200}
                boardWidth={boardWidth}
                position={game.fen()}
                onPieceDrop={onDrop}
                customBoardStyle={{
                    borderRadius: '4px',
                    boxShadow: '0 5px 15px rgba(0, 0, 0, 0.5)'
                }}
                ref={chessboardRef}
                arePiecesDraggable={game.turn() === color.charAt(0)}
                boardOrientation={color}
                areArrowsAllowed
            />
            {game.game_over() && <h5>Game Over</h5>}
        </div>
    );
}