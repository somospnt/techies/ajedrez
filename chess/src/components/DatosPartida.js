import React from 'react';
import { Piece } from "@chessire/pieces";

function DatosPartida({ nombre, creador, rival, observadores, turno, capturasBlanco, capturasNegro }) {

    const mostrarPiezasCapturadas = (color) => {
        const capturas = color === 'white' ? capturasBlanco : capturasNegro;
        return capturas.map((pieza, index) => {
            return <div style={{ marginLeft: '-17px' }} key={pieza + index}>
                <Piece color={color === 'white' ? 'black' : 'white'} piece={pieza.toUpperCase()} width={40} />
            </div>
        })
    };

    const mostrarDatosJugador = (jugador) => {
        return <div className={`row border border-5 rounded-3 ${turno === jugador.color?.charAt(0) && 'border-dark'}`}>
            <div className='col-3'>
                <Piece color={jugador.color ?? 'white'} piece="K" width={64} />
                <h4>{jugador.nombre}</h4>
            </div>
            <div className='col-9 d-flex align-items-center justify-content-start'>
                <div className='container p-0'>
                    <div className='col p-0 d-flex justify-content-start'>
                        {mostrarPiezasCapturadas(jugador.color)}
                    </div>
                </div>
            </div>
        </div>
    };

    return (
        <div className='col d-flex justify-content-center mt-5'>
            <div className='card shadow p-3 mb-5 bg-body rounded' style={{ width: '35rem' }}>
                {rival?.nombre
                    ?
                    <div className='container text-center'>
                        <h1 className='text-center'>{nombre}</h1>
                        {mostrarDatosJugador(creador)}
                        {mostrarDatosJugador(rival)}
                    </div>
                    : <p className='card-text text-center'>Esperando Rival</p>}
                {observadores.length !== 0 &&
                    <div>
                        <p className='card-text'>Observadores:</p>
                        {observadores.map((observador, index) => (
                            <li className='list-group-item'
                                key={observador.nombre + index}>
                                {observador.nombre}
                            </li>
                        ))}
                    </div>
                }
            </div>
        </div >
    )
}

export default DatosPartida;