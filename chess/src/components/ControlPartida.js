import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';

function ControlPartida({ socket, sala, rol }) {
    const [color, setColor] = useState('');
    const navigate = useNavigate();

    useEffect(() => {
        socket.on('color', setColor);
        return () => {
            socket.off('color');
        }
    }, []);

    return (
        <div className='col d-flex justify-content-center'>
            <div className='card shadow p-3 mb-5 bg-body rounded' style={{ width: '18rem' }}>
                <div className='text-center'>
                    {
                        rol === 'creador' && sala?.rival && <>
                            {!color &&
                                <>
                                    <p className='card-text'>Elegir Color</p>
                                    <div className='d-flex justify-content-center gap-3 mt-3'>
                                        <button
                                            className='btn btn-dark w-50'
                                            onClick={() => socket.emit('color', { color: 'white' })}>
                                            Blanco
                                        </button>
                                        <button
                                            className='btn btn-dark w-50'
                                            onClick={() => socket.emit('color', { color: 'black' })}>
                                            Negro
                                        </button>
                                    </div>
                                </>
                            }
                            {color &&
                                <div className='d-flex justify-content-center gap-3 mt-3'>
                                    {!sala?.rival && <button className='btn btn-primary w-50' onClick={() => { }}>Jugar</button>}
                                    <button
                                        className='btn btn-primary w-75'
                                        onClick={() => socket.emit('reset')}
                                    >
                                        Reiniciar Partida
                                    </button>
                                </div>
                            }
                        </>}</div>
                <div className='d-flex justify-content-center gap-3 mt-3'>
                    <button className='btn btn-primary w-75' onClick={() => {
                        socket.emit('salirSala');
                        navigate('/');
                    }}
                    >
                        Abandonar Sala</button>
                </div>
            </div>
        </div>
    )
}

export default ControlPartida;