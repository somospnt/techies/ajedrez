import React from 'react'
import { useNavigate } from 'react-router-dom';
import PlayVsPlay from './PlayVsPlay'

function CardPartida({ socket, tablero, nombre, creador, rival, observadores }) {
    const boardWidth = 200;
    const navigate = useNavigate();
    const entrarSala = (nombreSala, rol) => {
        socket.emit('entrarSala', { nombreSala, rol });
        navigate('/sala');
    };
    const onClickJugar = () => entrarSala(nombre, 'jugador');
    const onClickObservar = () => entrarSala(nombre, 'observador');

    return (
        <div className='col d-flex justify-content-center'>
            <div className='card shadow p-3 mb-5 bg-body rounded' style={{ width: '18rem' }}>
                <div className='card-body d-flex justify-content-center'>
                    {<PlayVsPlay socket={socket} tablero={tablero} boardWidth={boardWidth} />}
                </div>
                <div className='card-body text-center'>
                    <h5 className='card-title d-flex justify-content-center'>{nombre}</h5>
                    {rival?.nombre
                        ? <p className='card-text'>{creador.nombre} - {rival.nombre}</p>
                        : <p className='card-text'>{creador.nombre} está esperando rival</p>}
                    {observadores.length !== 0 &&
                        <div>
                            <p className='card-text'>Observadores:</p>
                            {observadores.map((observador) => (
                                <li className='list-group-item'
                                    key={observador.nombre + socket.id}>
                                    {observador.nombre}
                                </li>
                            ))}
                        </div>
                    }
                    <div className='d-flex justify-content-center gap-3 mt-3'>
                        {!rival && <button className='btn btn-primary w-50' onClick={onClickJugar}>Jugar</button>}
                        <button className='btn btn-primary w-50' onClick={onClickObservar}>Observar</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CardPartida;