import React from 'react'

function Registro({ onSubmit, inputOnChange, inputValue }) {
    return (
        <div className='col position-absolute top-50 start-50 translate-middle'>
            <div className='card shadow p-3 mb-5 bg-body rounded' style={{ width: '18rem' }}>
                <div className='card-body'>
                    <h5 className='card-title d-flex justify-content-center'>Registrate</h5>
                    <div className='d-flex justify-content-center gap-3 mt-3'>
                        <form onSubmit={onSubmit}>
                            <div className="mb-3">
                                <label htmlFor="recipient-name" className="col-form-label">Nombre</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="recipient-name"
                                    onChange={inputOnChange}
                                    value={inputValue}
                                />
                            </div>
                        </form>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-primary" onClick={onSubmit}>Entrar</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Registro;