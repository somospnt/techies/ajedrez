import { useEffect, useRef, useState } from 'react';
import Chess from 'chess.js';

import { Chessboard } from 'react-chessboard';
import GameOver from './GameOver';

export default function SquareStyles({ boardWidth, socket, tablero }) {
    const chessboardRef = useRef();
    const [game, setGame] = useState(new Chess(tablero));

    const [rightClickedSquares, setRightClickedSquares] = useState({});
    const [moveSquares, setMoveSquares] = useState({});
    const [optionSquares, setOptionSquares] = useState({});
    const [color, setColor] = useState('');
    const [mostrarModalFinJuego, setMostrarModalFinJuego] = useState(true);

    useEffect(() => {
        socket.on('color', setColor);
        socket.on('move', ({ sourceSquare, targetSquare }) => onDrop(sourceSquare, targetSquare));
        socket.on('reset', onClickReset);
        return () => {
            socket.off('move');
            socket.off('color');
            socket.off('reset');
        }
    }, []);

    function onClickReset() {
        safeGameMutate((game) => {
            game.reset();
        });
        chessboardRef.current.clearPremoves();
        setMostrarModalFinJuego(true);
    }

    function safeGameMutate(modify) {
        setGame((g) => {
            const update = { ...g };
            modify(update);
            return update;
        });
    }

    function onDrop(sourceSquare, targetSquare) {
        const gameCopy = { ...game };
        const move = gameCopy.move({
            from: sourceSquare,
            to: targetSquare,
            promotion: 'q' // always promote to a queen for example simplicity
        });
        setGame(gameCopy);
        // illegal move
        if (move === null) return false;
        setMoveSquares({
            [sourceSquare]: { backgroundColor: 'rgba(255, 255, 0, 0.4)' },
            [targetSquare]: { backgroundColor: 'rgba(255, 255, 0, 0.4)' }
        });
        move && color.charAt(0) === move.color && socket.emit('move', move, gameCopy.fen());
        return true;
    }

    function onMouseOverSquare(square) {
        if (color) {
            getMoveOptions(square);
        }
    }

    // Only set squares to {} if not already set to {}
    function onMouseOutSquare() {
        if (Object.keys(optionSquares).length !== 0) setOptionSquares({});
    }

    function getMoveOptions(square) {
        const moves = game.moves({
            square,
            verbose: true
        });
        if (moves.length === 0) {
            return;
        }

        const newSquares = {};
        moves.map((move) => {
            newSquares[move.to] = {
                background:
                    game.get(move.to) && game.get(move.to).color !== game.get(square).color
                        ? 'radial-gradient(circle, rgba(0,0,0,.1) 85%, transparent 85%)'
                        : 'radial-gradient(circle, rgba(0,0,0,.1) 25%, transparent 25%)',
                borderRadius: '50%'
            };
            return move;
        });
        newSquares[square] = {
            background: 'rgba(255, 255, 0, 0.4)'
        };
        setOptionSquares(newSquares);
    }

    function onSquareClick() {
        setRightClickedSquares({});
    }

    function onSquareRightClick(square) {
        const colour = 'rgba(0, 0, 255, 0.4)';
        setRightClickedSquares({
            ...rightClickedSquares,
            [square]:
                rightClickedSquares[square] && rightClickedSquares[square].backgroundColor === colour
                    ? undefined
                    : { backgroundColor: colour }
        });
    }

    return (
        <>
            <Chessboard
                id="SquareStyles"
                animationDuration={200}
                boardWidth={boardWidth}
                position={game.fen()}
                onMouseOverSquare={onMouseOverSquare}
                onMouseOutSquare={onMouseOutSquare}
                onSquareClick={onSquareClick}
                onSquareRightClick={onSquareRightClick}
                onPieceDrop={onDrop}
                customBoardStyle={{
                    borderRadius: '4px',
                    boxShadow: '0 5px 15px rgba(0, 0, 0, 0.5)'
                }}
                arePiecesDraggable={color}
                arePremovesAllowed
                isDraggablePiece={({ piece }) => piece.charAt(0) === (color.charAt(0) || 'w')}
                customSquareStyles={{
                    ...moveSquares,
                    ...optionSquares,
                    ...rightClickedSquares
                }}
                ref={chessboardRef}
                boardOrientation={color}
            />
            {
                game.game_over() && mostrarModalFinJuego &&
                <GameOver
                    ocultar={() => setMostrarModalFinJuego(false)}
                    ganador={game.turn() === 'b' ? 'Blancas' : 'Negras'}
                />
            }
        </>
    );
}