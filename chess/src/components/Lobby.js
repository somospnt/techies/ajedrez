import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import CardPartida from './CardPartida';

function Lobby({ socket }) {
    const [salas, setSalas] = useState([]);
    const navigate = useNavigate();

    useEffect(() => {
        socket.on('salas', setSalas);
        return () => socket.off('salas');
    }, []);

    const crearSala = (event) => {
        event.preventDefault();
        socket.emit('crearSala');
        navigate('/sala');
    };

    return (
        <>
            <button
                className="btn btn-success m-2 shadow rounded"
                type="button"
                onClick={crearSala}>
                Crear Sala
            </button>
            <div className='container'>
                <div className='row'>
                    {salas.map((sala) => {
                        return <CardPartida
                            key={socket.id}
                            {...sala}
                            socket={socket}
                        />
                    })}
                </div>
            </div>
        </>
    )
}

export default Lobby