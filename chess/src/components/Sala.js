import React, { useEffect, useState } from 'react';
import InfoPartida from './InfoPartida';
import PlayVsPlayWithHelpers from './PlayVsPlayWithHelpers';
import { useNavigate } from 'react-router-dom';

function Sala({ socket }) {
    const [sala, setSala] = useState({});
    const [existeSala, setExisteSala] = useState(false);
    const [jugador, setJugador] = useState({});
    const navigate = useNavigate();

    useEffect(() => {
        socket.on('sala', setSala);
        socket.on('datosJugador', setJugador);
        return () => {
            socket.off('sala');
            socket.off('datosJugador');
        }
    }, []);

    useEffect(() => {
        if (!sala) {
            alert('Tu rival Abandonó la Partida');
            navigate('/');
        } else {
            setExisteSala(Object.entries(sala).length !== 0);
        }
    }, [sala])

    return (
        existeSala &&
        <div className='container mt-5'>
            <div className='row'>
                <div className='col d-flex justify-content-center'>
                    <PlayVsPlayWithHelpers socket={socket} tablero={sala?.tablero} />
                </div>
                <div className='col'>
                    <InfoPartida socket={socket} sala={sala} rol={jugador.rol} />
                </div>
            </div>
        </div>
    )
}

export default Sala;