import React from 'react';

function GameOver({ ocultar, ganador }) {

    return (
        <div className='modal  d-block' tabIndex='-1' role='dialog' >
            <div className='modal-dialog' role='document'>
                <div className='modal-content'>
                    <div className='modal-header'>
                        <h5 className='modal-title'>Juego finalizado</h5>
                        <button className='btn-close' aria-label='Close' onClick={ocultar}>
                            <span aria-hidden='true'>&times;</span>
                        </button>
                    </div>
                    <div className='modal-body'>
                        <p>Ganan las {ganador}!!</p>
                        <p>Buena partida!!</p>
                    </div>
                    <div className='modal-footer'>
                        <button type='button' className='btn btn-primary' onClick={ocultar}>Aceptar</button>
                    </div>
                </div>
            </div>
        </div >
    )
}

export default GameOver;