import { io } from 'socket.io-client';
import Lobby from './components/Lobby';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useEffect, useState } from 'react';
import Sala from './components/Sala';
import 'bootstrap/dist/css/bootstrap.min.css';
import Registro from './components/Registro';
const socket = io('https://gambito-de-dami.herokuapp.com/');

function App() {
  const [nombre, setNombre] = useState('');
  const [registrado, setRegistrado] = useState(false);

  useEffect(() => {
    const nombreStorage = localStorage.getItem('nombre');
    if (nombreStorage) {
      setNombre(nombreStorage);
      setRegistrado(true);
      socket.emit('nombre', nombreStorage);
    };
  }, []);

  const registrar = (event) => {
    event.preventDefault();
    if (nombre !== '') {
      setRegistrado(true);
      socket.emit('nombre', nombre);
      localStorage.setItem('nombre', nombre);
      window.location = '/';
    }
  };

  return <>
    <nav className="navbar bg-light" >
      <div className="container-fluid">
        <a className="navbar-brand" href='/'>Gambito de Dami</a>
        <button
          className="btn btn-outline-success"
          onClick={() => { setRegistrado(false) }}>
          Cambiar Nombre
        </button>
      </div>
    </nav>
    {
      registrado ? (<>
        <BrowserRouter>
          <Routes>
            <Route path='/' element={<Lobby socket={socket} />} />
            <Route path='/home' element={<Lobby socket={socket} />} />
            <Route path='/sala' element={<Sala socket={socket} />} />
          </Routes>
        </BrowserRouter>
      </>) : (<>
        <Registro
          onSubmit={registrar}
          inputValue={nombre}
          inputOnChange={(e) => setNombre(e.target.value)}
        />
      </>
      )
    }
  </>
}

export default App;
