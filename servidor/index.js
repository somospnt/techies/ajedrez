import express from 'express';
import { Server as SocketServer } from 'socket.io';
import http from 'http';
import cors from 'cors';
const port = process.env.PORT || 4000;
const app = express();
const server = http.createServer(app);
const io = new SocketServer(server, { cors: { origin: '*' } });
let cantidadSalas = 1;
let salas = [];

io.on('connection', async (socket) => {
    console.log(`Se conecto ${socket.id}`);
    socket.on('move', function (move, tablero) {
        const sala = salas.find(sala => sala.nombre === socket.data.sala);
        if (move.captured) {
            if (move.color === 'w') {
                sala.capturasBlanco.push(move.captured);
            } else {
                sala.capturasNegro.push(move.captured);
            }
        }
        sala.tablero = tablero;
        move.color === 'w' ? sala.turno = 'b' : sala.turno = 'w';
        socket.broadcast.to(socket.data.sala).emit('move', { sourceSquare: move.from, targetSquare: move.to });
        io.to(sala.nombre).emit('sala', sala);
    });
    socket.on('color', function ({ color }) {
        const sala = salas.find(sala => sala.nombre === socket.data.sala);
        if (color === 'white') {
            sala.rival.color = 'black';
            socket.to(sala.rival.id).emit('color', 'black');
        } else if (color === 'black') {
            sala.rival.color = 'white';
            socket.to(sala.rival.id).emit('color', 'white');
        }
        socket.data.color = color;
        io.to(socket.id).emit('color', color);
        sala.creador = socket.data;
        io.to(sala.nombre).emit('sala', sala);
    });
    socket.on('reset', function () {
        io.to(socket.data.sala).emit('reset');
        io.to(socket.data.sala).emit('color', '');
        const sala = salas.find(sala => sala.nombre === socket.data.sala);
        sala.capturasBlanco = [];
        sala.capturasNegro = [];
        sala.tablero = 'start';
        io.to(sala.nombre).emit('sala', sala);
    });
    socket.on('nombre', nombre => {
        socket.data.nombre = nombre;
        io.to(socket.id).emit('salas', salas);
    });
    socket.on('crearSala', () => {
        const nombre = `Sala${cantidadSalas}`;
        cantidadSalas++;
        entrarEnSala(socket, nombre);
        actualizarDatosDelUsuario(socket, 'creador', nombre);
        salas.push(
            {
                nombre,
                creador: { ...socket.data, id: socket.id },
                observadores: [],
                turno: 'w',
                capturasBlanco: [],
                capturasNegro: []
            }
        );
        enviarEventos(socket);
    });
    socket.on('entrarSala', ({ nombreSala, rol }) => {
        entrarEnSala(socket, nombreSala);
        actualizarDatosDelUsuario(socket, rol, nombreSala);
        const sala = salas.find(sala => sala.nombre === nombreSala);
        if (rol === 'jugador') {
            sala.rival = { ...socket.data, id: socket.id };
            socket.data.rival = sala.creador;
        } else {
            sala.observadores = [...sala.observadores, { ...socket.data, id: socket.id }];
        }
        enviarEventos(socket);
    });
    socket.on('salirSala', () => salirSala(socket));
    socket.on('disconnect', () => salirSala(socket));
});

const salirSala = (socket) => {
    if (socket.data.rol === 'observador') {
        salas = salas.map(sala => {
            if (sala.nombre === socket.data.sala) {
                sala.observadores = sala.observadores.filter(({ nombre }) => nombre !== socket.data.nombre);
                return sala;
            }
        });
    } else {
        salas = salas.filter(sala => sala.nombre !== socket.data.sala);
    }
    console.log(`${socket.data.nombre} salio de la ${socket.data.sala}`);
    io.emit('salas', salas);
    io.to(socket.data.sala).emit('sala', salas.find(sala => sala.nombre === socket.data.sala));
};
const entrarEnSala = (socket, nombreSala) => {
    socket.rooms.forEach(room => {
        if (socket.id !== room) {
            socket.leave(room);
        }
    });
    socket.join(nombreSala);
};
const actualizarDatosDelUsuario = (socket, rol, nombreSala) => {
    socket.data = { ...socket.data, rol, sala: nombreSala };
};
const enviarEventos = (socket) => {
    io.to(socket.id).emit('datosJugador', socket.data);
    io.to(socket.data.sala).emit('sala', salas.find(sala => sala.nombre === socket.data.sala));
    io.emit('salas', salas);
};

app.use(cors());

server.listen(port);

console.log(`New Server started on port ${port}`);